function doGet(e) {
    var template = HtmlService.createTemplateFromFile('Index'); // It will create HTMl page from Index.html file data.
    var pageData= template.evaluate()
    .setTitle('Web App 1') // Set Title 
    .setFaviconUrl('https://www.gstatic.com/images/icons/material/product/2x/apps_script_64dp.png')
    .setSandboxMode(HtmlService.SandboxMode.IFRAME) //This method now has no effect — previously it set the sandbox mode used for client-side scripts
    .addMetaTag('viewport', 'width=device-width, initial-scale=1') // It is very important tag for Responsive 
    .setXFrameOptionsMode(HtmlService.XFrameOptionsMode.ALLOWALL) // Sets the state of the page's X-Frame-Options header, which controls clickjacking prevention.
     return pageData;
}